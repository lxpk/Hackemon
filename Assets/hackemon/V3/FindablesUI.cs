﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
namespace FindableV3
{
public class FindablesUI : MonoBehaviour {
	
	public static FindablesUI Instance;
	
	public class ChoiceButton 
	{
		public string name = "";
		public int uid = 0;
	}
	
	public Text text;
	public string textString;
	
	public void AddText(string message)
	{
		textString += ("\n" + message);
		text.text = textString;
	}
	
	public GameObject dialogGO;
	public Text dialogText;
	public Text dialogTitleText;
	public Button dialogDismissButton;
	public Text locationText;
	
	public GameObject choiceMenuGO; // the parent of choice buttons with a VerticalLayoutGroup
	public GameObject buttonPrefabGO; // the disabled "template" button
	public Text promptText;
	
	public void ShowChoices(string choicePrompt, List<ChoiceButton> choiceButtons, GameObject messageTargetGO, string messageName)
	{
		HideDialog();
		choiceMenuGO.SetActive(true);
		promptText.text = choicePrompt;
		
		Transform[] children = choiceMenuGO.GetComponentsInChildren<Transform>();
		foreach (Transform childToBeEaten in children) 
		{
			if (childToBeEaten != choiceMenuGO.transform && childToBeEaten != promptText.transform) {
				GameObject.Destroy(childToBeEaten.gameObject);
			}
		}
		int choiceIndex = 0;
		foreach (ChoiceButton choiceButton in choiceButtons)
		{
			GameObject newButtonGO = GameObject.Instantiate(
				buttonPrefabGO,
				buttonPrefabGO.transform.position,
				buttonPrefabGO.transform.rotation,
				buttonPrefabGO.transform.parent
			) as GameObject;
			newButtonGO.SetActive(true);
			newButtonGO.GetComponentInChildren<Text>().text = choiceButton.name;
			string choiceString = choiceButton.name;
			ChoiceButton button = choiceButton;
			newButtonGO.GetComponentInChildren<Button>().onClick.AddListener(
				() => ClickChoiceButton(button.uid, messageTargetGO, messageName)
			);
			choiceIndex++;
		}
	}
	
	public void HideChoices()
	{
		choiceMenuGO.SetActive(false);
	}
	
	public void ClickChoiceButton(int buttonUID, GameObject messageTargetGO, string messageName)
	{
		Debug.Log(buttonUID);
		messageTargetGO.SendMessage(messageName,buttonUID);
		HideChoices();
	}
	
	public void ClickDialogButton(string buttonName, GameObject messageTargetGO, string messageName)
	{
		Debug.Log(buttonName);
		messageTargetGO.SendMessage(messageName,buttonName);
		HideDialog();
	}
	
	public void ShowDialog(string dialogTitle, string dialogMessage, GameObject messageGO, string messageName)
	{
		HideChoices();
		dialogGO.SetActive(true);
		dialogTitleText.text = dialogTitle;
		dialogText.text = dialogMessage;
		string buttonName = "OK";
		dialogDismissButton.onClick.AddListener( () => ClickDialogButton(buttonName,messageGO,messageName));
	}
	
	public void HideDialog()
	{
		dialogGO.SetActive(false);
	}
	
	
	
	
	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
}