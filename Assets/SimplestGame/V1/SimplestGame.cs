﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SimplestGameV1 {
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// == VARIABLES
	/// === INT
	/// points
	/// === BOOL
	/// win
	/// == FUNCTION
	/// CheckGoal()
	/// == ENUM
	/// KeyCode
	/// == UNITY API CLASSES
	/// Input
	/// == UNITY API CLASS FUNCTIONS
	/// Input.GetKeyDown
	/// == DOT NOTATION
	/// 
	/// == CLASSES
	/// Player
	/// == COLLECTIONS
	/// == FOREACH
	/// == 
	/// </remarks>
	public class SimplestGame : MonoBehaviour {
		public string version = "1.0";
		public int points = 0;
		public int pointsGoal = 10;
		public bool someoneWon = false;
		
		void CheckGoal()
		{
			foreach (Player p in players)
			{
				if (p.points > pointsGoal)
				{
					someoneWon = true;
					Debug.Log(p.key.ToString() + " won!");
				}
			}
		}
		
		[System.Serializable]
		public class Player {
			public KeyCode key;
			public int points;
		}
		
		public List<Player> players = new List<Player>();
		
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () 
		{
			foreach (Player thisPlayer in players)
			{
				if (Input.GetKeyDown(thisPlayer.key))
				{
					thisPlayer.points++;
				}
			}
			CheckGoal();
		}
	}
}
